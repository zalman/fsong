import os
import wx
from time import clock
import wx.media
import wx.lib.buttons as buttons

import strings as s
import index_window
import find_window
import find

APP_EXIT = 1

dirName = os.path.dirname(os.path.abspath(__file__))
bitmapDir = os.path.join(dirName, 'bitmaps')
selectedDir = 'D:/'
path = ''
current_music = ''


class Main(wx.Frame):
    song_paths = {}

    def __init__(self, parent, _id):
        wx.Frame.__init__(self, parent, _id, s.APP_NAME, size=(640, 480),
                          style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
        self.Center()
        self.init_ui(parent, _id)

        self.SetBackgroundColour('#ffffff')
        self.index = 0

    def init_ui(self, parent=None, _id=None):
        self.populate_window()
        self.create_menu()

    def populate_window(self):
        panel = wx.Panel(self, -1)

        image_file = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//back2.jpg'))
        bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, bmp1, (0, 0))
        panel.SetBackgroundColour('#ffffff')

        font = wx.Font(15, wx.MODERN, wx.ITALIC, wx.ITALIC, False, u'Consolas')
        #        font2 = wx.Font(12, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(panel)

        vbox = wx.BoxSizer(wx.HORIZONTAL)

        label2 = wx.StaticText(panel, label=s.MATCH, pos=(30, 80))
        label2.SetFont(font)
        label2.SetForegroundColour('#000000')
        """
        sampleList = ['A.mp3', 'B.mp3', 'C.mp3', 'E.mp3', 'F.mp3']

        listBox = wx.ListBox(panel, -1, (30, 110), (470, 200), sampleList, wx.LB_SINGLE)
        listBox.SetSelection(3)
        """

        self.list_ctrl = wx.ListCtrl(panel, size=(470, 200), pos=(30, 110), style=wx.LC_REPORT | wx.BORDER_SUNKEN)
        self.list_ctrl.InsertColumn(0, 'Title', width=300)
        self.list_ctrl.InsertColumn(1, 'Nr. of matches', width=120)

        img = wx.Bitmap(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//rec.png')))
        img = self.scale_bitmap(img, 80, 80)
        self.recBtn = buttons.GenBitmapToggleButton(panel, bitmap=img, name="stop", pos=(530, 225), style=wx.NO_BORDER)
        self.recBtn.SetBitmapSelected(img)
        self.recBtn.SetBackgroundColour('#0BB5FF')

        vbox.Add(self.recBtn)

        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.list_click, self.list_ctrl)
        self.Bind(wx.EVT_BUTTON, self.rec_click, self.recBtn)

        self.SetAutoLayout(True)
        self.SetSizer(box)
        self.Layout()

    def rec_click(self, evt):
        global top_songs_list

        self.list_ctrl.DeleteAllItems()
        self.index = 0
        print "Recording..."

        mic_input_time = 20
        start_time = clock()
        try:
            top_songs_list = find.identify_from_mic(mic_input_time)
        except IOError:
            print "Mic input failed."

        if top_songs_list is None:
            self.add_line("The song could not be found", -1)
        else:
            print "Done!"
            print "Total time: ", clock() - start_time

            for song in top_songs_list:
                print song.track_name
                print song.chain_len
                self.add_line(song.track_name, song.chain_len)
                self.song_paths[song.track_name] = song.path

    def list_click(self, evt):
        pos = self.list_ctrl.GetFocusedItem()
        track_name = self.list_ctrl.GetItemText(pos)

        song_path = self.song_paths[track_name] + '/' + track_name
        if not song_path.endswith('.mp3'):
            song_path += '.mp3'

        wx.MessageBox("Path of the song: " + song_path, 'Info', wx.OK | wx.ICON_INFORMATION)

    def add_line(self, title, nr):
        nr = str(nr)
        self.list_ctrl.InsertStringItem(self.index, nr)
        self.list_ctrl.SetStringItem(self.index, 0, title)
        self.list_ctrl.SetStringItem(self.index, 1, nr)
        self.index += 1

    def on_erase_background(self, evt):
        print "test"
        dc = evt.GetDC()

        if not dc:
            dc = wx.ClientDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRect(rect)
        dc.Clear()
        bmp = wx.Bitmap(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//back.jpg')))
        dc.DrawBitmap(bmp, 0, 0)

    def create_menu(self):
        """
        Creates a menu
        """
        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        menu_item_index = wx.MenuItem(file_menu, 2, '&Index songs\tCtrl+I')
        menu_item_exit = wx.MenuItem(file_menu, APP_EXIT, '&Quit\tCtrl+Q')
        #menu_item_test = wx.MenuItem(file_menu, APP_EXIT, '&Test')
        menu_item_find = wx.MenuItem(file_menu, 3, '&Find song\tCtrl+F')

        bitmap_i = wx.Bitmap(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//index.jpg')))
        bitmap_e = wx.Bitmap(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//exit.png')))
        bitmap_i = self.scale_bitmap(bitmap_i, 20, 20)
        bitmap_e = self.scale_bitmap(bitmap_e, 20, 20)
        menu_item_exit.SetBitmap(bitmap_e)
        menu_item_index.SetBitmap(bitmap_i)

        file_menu.AppendItem(menu_item_index)
        file_menu.AppendItem(menu_item_find)
        file_menu.AppendItem(menu_item_exit)

        menubar.Append(file_menu, '&File')

        self.SetMenuBar(menubar)

        self.Bind(wx.EVT_MENU, self.on_click_index, menu_item_index)
        self.Bind(wx.EVT_MENU, self.on_click_find, menu_item_find)
        self.Bind(wx.EVT_MENU, self.quit_app, menu_item_exit)

    @staticmethod
    def on_click_find(event):
        new_app = wx.App()
        new_frame = find_window.Findwindow(None, title=s.INDEX_WINDOW)
        new_frame.Show()
        new_app.MainLoop()

    @staticmethod
    def on_click_index(event):
        new_app = wx.App()
        new_frame = index_window.IndexWindow(None, title=s.INDEX_WINDOW)
        new_frame.Show()
        new_app.MainLoop()

    @staticmethod
    def scale_bitmap(bitmap_to_resize, width, height):
        """
        Scales an image to an image with size: width*height
        """
        image = wx.ImageFromBitmap(bitmap_to_resize)
        image = image.Scale(width, height, wx.IMAGE_QUALITY_HIGH)
        result = wx.BitmapFromImage(image)
        return result

    def quit_app(self, e):
        """
        Quits the application
        """
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    frame = Main(parent=None, _id=-1)
    frame.Show()
    app.MainLoop()
