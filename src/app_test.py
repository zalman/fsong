import indexer as indexer_module
import storage
import find

def find_song():

    print "Searching..."

    MIC_INPUT_TIME = 20
    top_songs_list =  []
    try:
        top_songs_list = find.identify_from_mic(MIC_INPUT_TIME)
    except IOError:
        print "Mic input failed."

    if top_songs_list is []:
       print "The song could not be found" 
    else:
        print "Top 5 matches -- track_name, path, artist, exact_matches"
        for song in top_songs_list:
            print song.track_name, \
                  song.path, \
                  song.artist, \
                  song.chain_len

def get_songs_in_db():
    hash_store = storage.HashStore()
    songs_in_db = hash_store.get_all_songs()
    for song in songs_in_db:
        print song.track_name, \
              song.path, \
              song.artist
    
def index_dir():
    indexer = indexer_module.Indexer()
    DIR_TO_INDEX = '/media/zalman/DATA/MUZICA/'

    print indexer.get_num_of_files(DIR_TO_INDEX)
    print indexer.index_dir(DIR_TO_INDEX)
    
def reset_db():
    hash_store = storage.HashStore()
    hash_store.create_empty_tables()

if __name__ == '__main__':
    reset_db()
    #index_dir()
    #get_songs_in_db()
    #find_song()
    hash_store = storage.HashStore()
    print hash_store.get_no_of_songs()

