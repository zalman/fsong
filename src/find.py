import pyaudio
from pyaudio import PyAudio
import threading
import Queue
import time
from collections import namedtuple

import storage
import indexer

Song = namedtuple('Song', ('track_name', 'path', 'artist', 'chain_len'))


class Finder(threading.Thread):
    def __init__(self):
        self.queue = Queue.Queue()
        self.output = None
        self.abandoned = False
        threading.Thread.__init__(self)
        self.daemon = True

    def push_data(self, new_time, buffer_data):
        self.queue.put((new_time, buffer_data), False)

    def abandon(self):
        self.abandoned = True

    def run(self):
        hash_store = storage.HashStore()

        # Dict of part_chains, given as {song_id : [song_id, last_offset, time_in_song, chain_length]}
        part_chains = {}
        longest_chain = 0

        while not self.abandoned or not self.queue.empty():
            if self.queue.qsize() > 20:
                print "Large backlog: %d" % self.queue.qsize()

            input_time, input_data = self.queue.get()
            record_points = indexer.AudioPart.get_record_points(input_time, input_data)

            # Find and retrieve all parts from all songs that match
            # the hash of the current part from the mic input
            # The matching_parts is returned as a list of tuples that contain
            # ('time', 'hash', 'song_id')
            matching_parts = hash_store.get_hashes(record_points.hash())

            if matching_parts is not None:

                # Iterate through all the song parts from the database
                # that match the hash code of the current part in the 
                # recording and check if the time offset matches.
                # If the time offset matches than update the length of
                # the chain for that particular song and the offset 
                # information.
                for song_part in matching_parts:
                    handled = False
                    # Retrieve the chain of parts to whom the current
                    # part belongs
                    for chain in part_chains.get(song_part.song_id, []):
                        if abs((input_time - chain[1]) - (song_part.time - chain[2])) < 0.07:
                            chain[1] = input_time
                            chain[2] = song_part.time
                            chain[3] += 1
                            if chain[3] > longest_chain:
                                longest_chain = chain[3]
                                best_guess = chain[0]
                                # XXX: scoate output-ul in GUI
                                print "Current best (of %d partial matches): %d chain, %d - %s" % \
                                      (len(part_chains), longest_chain,
                                       best_guess, hash_store.get_song_by_id(chain[0]).track_name)

                            handled = True
                            break

                    # If the current part (hash, time) did not make into one of the existing
                    # chanins, add it to the part_chains
                    if not handled:
                        part_chains.setdefault(song_part.song_id, []).append(
                            [song_part.song_id, input_time, song_part.time, 1])

            # Make sure we are not hogging the GIL and blocking the mic read
            time.sleep(0)

            # Iterate through each chain and find the ones that have
            # length >= 7. If we find one => call it a winner.
            # Also, get the top five tracks in reverse order by the
            # length of the chain
            for song_chain in part_chains:
                for chain in part_chains[song_chain]:
                    if chain[3] >= 3:

                        sorted_matches = []
                        # Get the max chain from each dict entry in the part_chains dict
                        # and store the length of the song_id and chain_len
                        for sg_chain in part_chains:
                            max_len_chain = max(part_chains[sg_chain], key=lambda x: x[3])
                            sorted_matches.append([max_len_chain[0], max_len_chain[3]])

                        # Sort them in reverse order by the chain length
                        sorted_matches.sort(key=lambda x: x[1], reverse=True)

                        # Create a list of tuples which will contain the top songs
                        # in the form of  [(track_name, path, artist, chain_len)]
                        # Depending on how many matches are in the list, we will
                        # select 5 or less matches
                        top_songs = []
                        if len(sorted_matches) >= 5:
                            top_matches = sorted_matches[:5]
                        else:
                            top_matches = sorted_matches

                        for match in top_matches[:5]:
                            # Get the song from the database
                            song = hash_store.get_song_by_id(match[0])
                            chain_len = match[1]
                            top_songs.append(Song(track_name=song.track_name,
                                                  path=song.path,
                                                  artist=song.artist,
                                                  chain_len=chain_len))
                        self.output = top_songs
                        return


def identify_from_mic(max_time):
    input_stream = PyAudio().open(format=pyaudio.paInt16,
                                  channels=1,
                                  rate=44100,
                                  input=True,
                                  frames_per_buffer=4096)
    current_time = 0.0

    finder = Finder()
    finder.start()
    while current_time < max_time:
        data = input_stream.read(4096)
        finder.push_data(current_time, data)
        current_time += (4096 / 44100.0)

        if finder.output is not None:
            return finder.output

    finder.abandon()
    finder.join()
    if finder.output is not None:
        return finder.output

    return None
