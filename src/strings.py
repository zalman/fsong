APP_NAME = 'FSong'
FIND = 'Find song'
MATCH = 'Matches '
FIND_WINDOW = 'Find song'
INDEX_WINDOW = 'Index songs'
ROOT = 'Select music directory '
ROOT_BTN = 'Browse'
INDEX_LBL = 'Indexing report'
INDEX_BTN = 'Index Songs'
