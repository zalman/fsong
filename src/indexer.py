import os
import numpy
import audiotools
from audiotools import pcmconverter
from numpy import fft, fromstring, int16, abs
from time import clock

import storage


class Indexer(object):
    def __init__(self):
        self.hash_store = storage.HashStore()

    def index_and_store(self, file_name, file_path):
        new_song = Song.get_index_from_file(file_name, file_path)
        self.hash_store.store_song(new_song)

    @staticmethod
    def get_num_of_files(dir_path):
        if os.path.exists(dir_path):
            files_cnt = 0
            for root, bar, files in os.walk(dir_path):
                for _ in files:
                    files_cnt += 1

            return files_cnt
        else:
            return None
        
    def index_file(self, file_name, file_path):
        try:
            try:
                track_name = Song.get_song_metadata(file_name, file_path + '/', False)
            except:
                track_name = "wrong format."

            existing_tracks = self.hash_store.get_track_names()

            if track_name not in existing_tracks:
                self.index_and_store(file_name, file_path)
                return "Stored -- %s" % file_name
            else:
                return "'%s' already exists in the DB" % file_name
        except audiotools.UnsupportedFile:
            return "Skipped -- %s" % file_name
        except Exception, e:
            print e

    def index_dir(self, dir_path):
        global tmp_file_name

        indexed_files = 0
        indexing_report = []
        start_time = clock()
        existing_tracks = self.hash_store.get_track_names()

        for root, bar, files in os.walk(dir_path):
            for file_name in files:
                status = ""
                try:
                    # XXX: Scos din cauza exceptiilor. Totusi, adauga dupa ce 
                    # rezolvi partea cu testul.
                    #track_name = Song.get_song_metadata(file_name, root + '/', False)

                    #if track_name not in existing_tracks:
                    if True:
                        self.index_and_store(file_name, root)
                        existing_tracks.append(file_name)
                        indexed_files += 1
                        # XXX: adauga mesajul in grafica
                        status = "Stored -- %s" % file_name
                        tmp_file_name = root + '/' + file_name
                        print "Stored %s" % tmp_file_name
                    else:
                        status = "'%s' already exists in the DB" % file_name
                        print "'%s' already exists in the DB" % tmp_file_name
                except audiotools.UnsupportedFile:
                    status = "Skipped -- %s" % file_name
                    file_name = root + '/' + file_name
                    print "Skipped unsupported file %s" % file_name
                except Exception, e:
                    print e
                indexing_report.append(status)

        end_time = clock() - start_time
        return (indexing_report, end_time)


class Song(object):
    def __init__(self, name, path, artist, parts=None):
        self.track_name = name
        self.path = path
        self.artist = artist
        self.parts = parts

    @staticmethod
    def get_song_metadata(file_name, file_path, return_stream):
        stream = audiotools.open(file_path + '/' + file_name)

        metadata = stream.get_metadata()
        if metadata is not None:
            track_name = metadata.track_name if metadata.track_name is not None else file_name
            artist = metadata.artist_name
        else:
            track_name = file_name
            artist = None

        if return_stream:
            return (stream, track_name, artist)
        else:
            return track_name

    @classmethod
    def get_index_from_file(self, file_name, file_path):

        stream, track_name, artist = self.get_song_metadata(file_name,
                                                            file_path,
                                                            True)

        new_song = self(track_name, file_path, artist)

        # Extract PCM data
        pcm_data = stream.to_pcm()

        # We are only interested in one channel => extract mono data
        mono_data = audiotools.pcmconverter.Averager(pcm_data)
        # Process the mono data
        new_song.split_stream_into_parts(mono_data)

        return new_song

    def split_stream_into_parts(self, stream):
        self.parts = []
        time = 0.0
        while True:
            # Process 4096 bytes at a time
            output = stream.read(4096)
            # This tells us that we have reached the end
            if output.frames == 0:
                return
            bytes_data = output.to_bytes(not numpy.little_endian, True)
            if len(bytes_data) > 0:
                current_part = AudioPart.get_record_points(time, bytes_data)

                if current_part is not None:
                    self.parts.append(current_part)

            time += output.frames / float(stream.sample_rate)


class AudioPart(object):
    fuzz_factor = 2

    def __init__(self, time, record_points):
        self.time = time
        self.record_points = record_points 

    @classmethod
    def get_record_points(self, time, bytes_data):
        numpy_array = fromstring(bytes_data, dtype=int16)
        frequencies = abs(fft.rfft(numpy_array))
        # XXX: De ce < 181
        if len(frequencies) < 181:
            return None
        freq_bins = ((40, 80), (80, 120), (120, 180), (180, 300))
        return self(time, [AudioPart.get_max(frequencies, x[0], x[1]) for x in freq_bins])

    @staticmethod
    def get_max(r, a, b):
        return max([(x+a, r[a:b][x]) for x in xrange(len(r[a:b]))], key=lambda x: x[1])

    def hash(self):
        a, b, c, d = [rp[0] for rp in self.record_points]
        f = self.fuzz_factor
        return (((a - a % f) << 24) | ((b - b % f) << 16) | ((c - c % f) << 8) | (d - d % f)) & 0xffffffff

    def __repr__(self):
        return "AudioPart(%f, %s)" % (self.time, self.record_points)
