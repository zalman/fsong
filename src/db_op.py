import sqlite3 as lite

def create_tables():
    con = lite.connect('music.db') 

    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS songs")
        cur.execute("CREATE TABLE songs (track_name TEXT, artist TEXT)")
        cur.execute("DROP TABLE IF EXISTS song_chunks")
        cur.execute("CREATE TABLE song_chunks(time REAL, hash INT, song_id INT)")

def select_songs():
    con = lite.connect('music.db') 
    
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM songs")

        rows = cur.fetchall()
        for row in rows:
            print row 

def select_chunks():
    con = lite.connect('music.db')
    
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM song_chunks")

        rows = cur.fetchall()
        f = open('/home/zalman/Desktop/k.txt', 'w')
        for row in rows:
            f.write(str(row[0]) + " " + str(row[1]) + " " + str(row[2]) + "\n")

def get_song_info():
    i = raw_input("Index: ")
    con = lite.connect('music.db')

    with con:
        cur = con.cursor()
        cur.execute("SELECT track_name, artist FROM songs where rowid = ?", (i,))
        data = cur.fetchone()
        if data:
            print data[0], data[1]

def get_song_hash():
    i = raw_input("Index: ")
    con = lite.connect('music.db')

    with con:
        cur = con.cursor()
        cur.execute("SELECT * from song_chunks where song_id = ?", (i,))

        data = cur.fetchall()
        if data:
            for  row in data:
                print row[0], row[1], row[2]

def get_average_hash_num():
    
    total_hashes = 0
    con = lite.connect('music.db')
    with con:
        cur = con.cursor()
        for i in range(50):
            cur.execute("SELECT * from song_chunks where song_id = ?", (i,))
            data = cur.fetchall()
            total_hashes += len(data)

    print "NUmber of hashes average: ", total_hashes/50


if __name__ == '__main__':
    options = {1: create_tables,
               2: select_songs, 
               3: select_chunks,
               4: get_song_info,
               5: get_song_hash, 
               6: get_average_hash_num}
    opt_num = int(raw_input("Optiune: "))
    
    options[opt_num]()

