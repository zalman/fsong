
import wx
import strings as s
import os

import storage


class Findwindow(wx.Frame):
  
    def __init__(self, parent, title):

        hash_store = storage.HashStore()
        self.tracks_list = hash_store.get_track_names()
        super(Findwindow, self).__init__(parent, title=title, size=(480, 360),
                                         style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
            
        self.Centre()
        self.Show()
        self.init_ui(parent, id)

    def init_ui(self, parent, id):
        self.populate_window(self.tracks_list)
        
    def populate_window(self, sample_list=None):
        if not sample_list:
            sample_list = []
        panel = wx.Panel(self, -1)
        
        image_file = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//back.jpg'))
        bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, bmp1, (0, 0))
        panel.SetBackgroundColour('#ffffff')     
        
        font = wx.Font(15, wx.MODERN, wx.ITALIC, wx.ITALIC, False, u'Consolas')               
        font2 = wx.Font(12, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
        
        label1 = wx.StaticText(panel, label=s.FIND, pos=(30, 30))
        label1.SetFont(font)
        label1.SetForegroundColour('#000000')
        
        self.txt_title = wx.TextCtrl(panel, pos=(30, 65), size=(420, 30))
        self.txt_title.SetFont(font2)

        self.listBox = wx.ListBox(panel, -1, (30, 130), (420, 200), sample_list, wx.LB_SINGLE)
        #self.listBox.SetSelection(3)
        
        self.Bind(wx.EVT_TEXT, self.text_changed, self.txt_title)
        
        box = wx.BoxSizer(wx.VERTICAL)        
        box.Add(panel)

        self.SetAutoLayout(True)
        self.SetSizer(box)                  
        self.Layout() 
        
    def text_changed(self, event):
        text = self.txt_title.GetValue()

        self.listBox.Clear()
        for track in self.tracks_list:
            if text.lower() in track.lower():
                self.listBox.Append(track)
       
if __name__ == '__main__':
  
    app = wx.App()
    frame = Findwindow(None, title=s.FIND_WINDOW)
    frame.Show()
    app.MainLoop()
