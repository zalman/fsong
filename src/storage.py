import sqlite3
from collections import namedtuple
import indexer

PartHash = namedtuple('PartHash', ('time', 'hash', 'song_id'))


class HashStore(object):
    def __init__(self):
        # TO: schimba locatia fisierului .db
        self.conn = sqlite3.connect("song_indexes.db")

    def create_empty_tables(self):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("DROP TABLE IF EXISTS songs")
            db_cursor.execute("CREATE TABLE songs (track_name TEXT, path TEXT, artist TEXT)")
            db_cursor.execute("DROP TABLE IF EXISTS song_hashes")
            db_cursor.execute("CREATE TABLE song_hashes (time REAL, hash INT, song_id INT)")

    def store_song(self, song):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("INSERT INTO songs (track_name, path, artist) VALUES (?, ?, ?)",
                              (song.track_name, song.path, song.artist))
            song_id = db_cursor.lastrowid
            db_cursor.executemany("INSERT INTO song_hashes (time, hash, song_id) VALUES (?, ?, ?)",
                                  [(part.time, part.hash(), song_id) for part in song.parts])

            self.conn.commit()

    def get_hashes(self, hash_code):
        with self.conn:
            db_cursor = self.conn.cursor()
            matching_hashes = []
            for row in db_cursor.execute("SELECT time, hash, song_id FROM song_hashes WHERE hash = ?", (hash_code,)):
                matching_hashes.append(PartHash(time=row[0], hash=row[1], song_id=row[2]))

            return matching_hashes

    def get_song_by_id(self, row_id):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("SELECT track_name, path, artist FROM songs WHERE rowid = ?", (row_id, ))
            data = db_cursor.fetchone()
            if data is None:
                return None
            return indexer.Song(data[0], data[1], data[2])

    def get_track_names(self):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("SELECT track_name FROM songs")
            songs = db_cursor.fetchall()
            track_names = []
            if songs:
                for song in songs:
                    track_names.append(song[0])

            return track_names

    def get_all_songs(self):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("SELECT * FROM songs")
            songs = db_cursor.fetchall()

            if songs is not None:
                songs_list = []
                for song in songs:
                    songs_list.append(indexer.Song(song[0], song[1], song[2]))
                return songs_list
            else:
                return None

    def get_no_of_songs(self):
        with self.conn:
            db_cursor = self.conn.cursor()
            db_cursor.execute("SELECT * FROM songs")
            songs = db_cursor.fetchall()

            return len(songs)
