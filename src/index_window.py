import os
import wx
import wx.lib.newevent
from time import clock
import strings as s

import indexer as indexer_module


class IndexWindow(wx.Frame):

    res = []
    progress_range = 0
    progress_value = 0

    NewSongIndex, EVT_NEW_SONG_INDEX = wx.lib.newevent.NewEvent()
  
    def __init__(self, parent, title):
        super(IndexWindow, self).__init__(parent, title=title, size=(480, 640),
                                          style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
            
        self.Centre()
        self.Show()
        self.init_ui(parent, id)

        self.selected_dir = ''
        self.index = 0

    def init_ui(self, parent, id):
        self.populate_window()
        
    def populate_window(self):
        panel = wx.Panel(self, -1)
        
        image_file = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'img//back3.jpg'))
        bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        wx.StaticBitmap(panel, -1, bmp1, (0, 0))
        panel.SetBackgroundColour('#ffffff')     
        
        font = wx.Font(15, wx.MODERN, wx.ITALIC, wx.ITALIC, False, u'Consolas')               
        font2 = wx.Font(12, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
        
        label1 = wx.StaticText(panel, label=s.ROOT, pos=(30, 30))
        label1.SetFont(font)
        label1.SetForegroundColour('#000000')
        
        self.txt_root = wx.TextCtrl(panel, pos=(30, 65), size=(310, 30))
        self.txt_root.SetFont(font2)
      
        self.btn_get_root = wx.Button(panel, label= s.ROOT_BTN, size=(100, 30), pos=(355, 65))
        
        self.btn_index = wx.Button(panel, label= s.INDEX_BTN, size=(420, 30), pos=(30, 110))
        
        box = wx.BoxSizer(wx.VERTICAL)        
        box.Add(panel)
        
        label2 = wx.StaticText(panel, label=s.INDEX_LBL, pos=(30, 150))
        label2.SetFont(font)
        label2.SetForegroundColour('#000000')

        self.list_ctrl = wx.ListCtrl(panel, pos=(30, 180), size=(425, 415), style=wx.LC_REPORT | wx.BORDER_SUNKEN)
        self.list_ctrl.InsertColumn(0, 'Title', width=400)
        self.Bind(self.EVT_NEW_SONG_INDEX, self.update)
        
        self.progress = wx.Slider(panel, size=(425,20), pos = (30, 605))
        
        self.Bind(wx.EVT_BUTTON, self.get_root_click, self.btn_get_root)
        self.Bind(wx.EVT_BUTTON, self.index_click, self.btn_index)
        
        self.SetAutoLayout(True)
        self.SetSizer(box)                  
        self.Layout() 

    def update(self, event):
        for value in self.res:
            if value is not None:
                print value
                self.add_line(value)
        self.progress.SetValue(self.progress_value)

    def add_line(self, title):
        self.list_ctrl.InsertStringItem(self.index, title)
        self.list_ctrl.SetStringItem(self.index, 0, title) 
        self.index += 1
        
    def get_root_click(self, event):
        user_path = 'c:/'
        dialog = wx.DirDialog(None, "Please choose your music directory:", style=1, defaultPath=user_path, pos=(10, 10))
        #global selectedDir
        if dialog.ShowModal() == wx.ID_OK:
            self.selected_dir = dialog.GetPath()
        dialog.Destroy()
        #print self.selectedDir
        self.txt_root.SetValue(self.selected_dir)
    
    @staticmethod
    def show_message_exclamation(message):
        wx.MessageBox(message, 'Warning', wx.OK | wx.ICON_EXCLAMATION)

    @staticmethod
    def show_message_info(message):
        wx.MessageBox(message, 'Info', wx.OK | wx.ICON_INFORMATION)

    def index_click(self, event):
        print "Indexing..."

        if self.selected_dir == '':
            self.show_message_exclamation("You have to select a directory first")
        else:
            print self.selected_dir
            indexer = indexer_module.Indexer()
            num_files = indexer.get_num_of_files(self.selected_dir)
            indexed_files = 0
            self.progress.SetRange(0, num_files)
            start_time = clock()
            files_cnt = 0

            for root, bar, files in os.walk(self.selected_dir):
                for file_name in files:
                    files_cnt += 1
                    index_status = indexer.index_file(file_name, root)
                    indexed_files += 1
                    self.progress.SetValue(indexed_files)
                    self.res = []
                    self.res.append(index_status)
                    wx.Yield()
                    event = self.NewSongIndex()
                    self.GetEventHandler().ProcessEvent(event)

            self.show_message_exclamation("Indexing complete!")

            print "Nr. of files: ", files_cnt
            print "Total time: ", clock()-start_time

    
if __name__ == '__main__':
  
    app = wx.App()
    frame = IndexWindow(None, title=s.INDEX_WINDOW)
    frame.Show()
    app.MainLoop()
